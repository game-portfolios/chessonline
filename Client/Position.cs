﻿using System.Collections.Generic;

namespace ChessOnline
{
    [System.Serializable]
    public class Position
    {
        public static List<Position> List { get; } = new List<Position>();

        public char this[char index]
        {
            get
            {
                return X;
            }
            set
            {
                X = (char)(index + value);
            }
        }
        public int this[int index]
        {
            get
            {
                return Y;
            }
            set
            {
                Y = index + value;
            }
        }

        public static Position Get(char x, int y)
        {
            const int width = 8;
            return List.Get(width, x, y);
        }

        public char X { get; set; } = 'A';
        public int Y { get; set; } = 1;

        public override string ToString()
        {
            return X + Y.ToString();
        }

        public static implicit operator string(Position pos)
        {
            return pos.ToString();
        }

        public static implicit operator Position(string pos)
        {
            return new Position(pos[0], int.Parse(pos.Substring(1)));
        }

        public static char operator +(Position pos, char x)
        {
            return pos.X += x;
        }

        public Position(char x, int y)
        {
            if (List.Count == 64)
            {
                List.RemoveAll(obj => true);
            }

            X = x;
            Y = y;
            List.Add(this);
        }
    }
}
