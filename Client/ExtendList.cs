﻿using System.Collections.Generic;

public static class ExtendList
{
    public static T Get<T>(this IList<T> list, int width, int x, int y) 
    {
        return list[y * width + x];
    }
}