﻿using System.Collections.Generic;

namespace ChessOnline
{
    public class Renderer
    {
        public Dictionary<Position, Piece> Pieces { get; private set; } 

        public static Renderer Get { get; }

        public void Remove(Position position, Piece piece)
        {
        }

        public void Move(Position position, Piece piece)
        {
          
        }

        public void ResetPiece() 
        {
            Renderer.Get.Pieces = new Dictionary<Position, Piece>();
        }

        static Renderer() 
        {
            Get = new Renderer
            {
            };
        }
    }
}