let _default = true, target, x, y;

window.clickPiece = (e, X, Y) => {
  console.log('piece clicked!');

  let _target = document.querySelector(`.${X + Y}`);
  x, y = X, Y;

  if (_default) {
    _default = false;
    target = document.querySelector(`.${X + Y}`);
    target.style.border = '2px solid #f1c40f';
  } else {
    let std = target === _target;
    _target.style.border = '2px solid #f1c40f';
    target.style.border = std ? '2px solid #f1c40f' : '';
    target = document.querySelector(`.${X + Y}`);
  }
};

window.initial = () => {
  console.log('init');
  const gridArray = document.querySelectorAll('.grid');
  console.log(gridArray);
  Object.keys(gridArray).map(i => {
    gridArray[i].addEventListener('click', () => {
      console.log('clicked~!');
    });
  });
};
